//
//  NSString+JavaAPI.m
//  Semulov
//
//  Created by Terry on 1/15/14.
//  Copyright (c) 2014 Kevin Wojniak. All rights reserved.
//

#import "NSString+JavaAPI.h"
#import <Foundation/Foundation.h>

@implementation NSString (JavaAPI)
- (bool) equals: (NSString*) comp {
    NSComparisonResult result = [self compare:comp];
    if (result == NSOrderedSame) {
        return YES;
    }
    return NO;
}

- (bool) equalsIgnoreCase: (NSString*) comp {
    return [[self lowercaseString] equals:[comp lowercaseString]];
}

- (bool) equalsWildcards:(NSString *)content {
    return compareWildcards([self UTF8String], [content UTF8String]);
}

bool matchWildcards(const char *pattern, const char *content) {
	// if we reatch both end of two string, we are done
	if ('\0' == *pattern && '\0' == *content)
		return true;
	/* make sure that the characters after '*' are present in second string.
     this function assumes that the first string will not contain two
     consecutive '*'*/
	if ('*' == *pattern && '\0' != *(pattern + 1) && '\0' == *content)
		return false;
	// if the first string contains '?', or current characters of both
    // strings match
	if ('?' == *pattern || *pattern == *content)
		return matchWildcards(pattern + 1, content + 1);
	/* if there is *, then there are two possibilities
     a) We consider current character of second string
     b) We ignore current character of second string.*/
	if ('*' == *pattern)
		return matchWildcards(pattern + 1, content) || matchWildcards(pattern, content + 1);
	return false;
}

bool compareWildcards(const char *pattern,const char *content) {
	if (NULL == pattern || NULL == content)
		return NO;
	return matchWildcards(pattern, content);
}

- (bool) contains: (NSString*) substring {
    NSRange range = [self rangeOfString:substring];
    return range.location != NSNotFound;
}

- (bool) endsWith: (NSString*) substring {
    int location = [self lastIndexOf:substring];
    return location == [self length] - [substring length];
}

- (bool) startsWith: (NSString*) substring {
    NSRange range = [self rangeOfString:substring];
    return range.location == 0;
}

- (int) indexOf: (NSString*) substring {
    NSRange range = [self rangeOfString:substring];
    return range.location == NSNotFound ? -1 : range.location;
}

- (int) indexOf:(NSString *)substring startingFrom: (int) index {
    NSString* test = [self substringFromIndex:index];
    return [test indexOf:substring];
}

- (int) lastIndexOf: (NSString*) substring {
    if (! [self contains:substring]) {
        return -1;
    }
    int matchIndex = 0;
    NSString* test = self;
    while ([test contains:substring]) {
        if (matchIndex > 0) {
            matchIndex += [substring length];
        }
        matchIndex += [test indexOf:substring];
        test = [test substringFromIndex: [test indexOf:substring] + [substring length]];
    }
    
    return matchIndex;
}

- (int) lastIndexOf:(NSString *)substring startingFrom: (int) index {
    NSString* test = [self substringFromIndex:index];
    return [test lastIndexOf:substring];
}

- (NSString*) substringFromIndex:(int)from toIndex: (int) to {
    NSRange range;
    range.location = from;
    range.length = to - from;
    return [self substringWithRange: range];
}

- (NSString*) trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSArray*) split: (NSString*) token {
    return [self split:token limit:0];
}

- (NSArray*) split: (NSString*) token limit: (int) maxResults {
    NSMutableArray* result = [NSMutableArray arrayWithCapacity: 8];
    NSString* buffer = self;
    while ([buffer contains:token]) {
        if (maxResults > 0 && [result count] == maxResults - 1) {
            break;
        }
        int matchIndex = [buffer indexOf:token];
        NSString* nextPart = [buffer substringFromIndex:0 toIndex:matchIndex];
        buffer = [buffer substringFromIndex:matchIndex + [token length]];
        if (nextPart) {
            [result addObject:nextPart];
        }
    }
    if ([buffer length] > 0) {
        [result addObject:buffer];
    }
    
    return result;
}

- (NSString*) replace: (NSString*) target withString: (NSString*) replacement {
    return [self stringByReplacingOccurrencesOfString:target withString:replacement];
}
@end
