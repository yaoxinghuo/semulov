//
//  NSString+JavaAPI.h
//  Semulov
//
//  Created by Terry on 1/15/14.
//  Copyright (c) 2014 Kevin Wojniak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JavaAPI)
- (bool) equals: (NSString*) comp;
- (bool) equalsIgnoreCase: (NSString*) comp;
- (bool) equalsWildcards:(NSString *)content;//自己本身是个pattern就是带* ?啥的
- (bool) contains: (NSString*) substring;
- (bool) endsWith: (NSString*) substring;
- (bool) startsWith: (NSString*) substring;
- (int) indexOf: (NSString*) substring;
- (int) indexOf:(NSString *)substring startingFrom: (int) index;
- (int) lastIndexOf: (NSString*) substring;
- (int) lastIndexOf:(NSString *)substring startingFrom: (int) index;
- (NSString*) substringFromIndex:(int)from toIndex: (int) to;
- (NSString*) trim;
- (NSArray*) split: (NSString*) token;
- (NSString*) replace: (NSString*) target withString: (NSString*) replacement;
- (NSArray*) split: (NSString*) token limit: (int) maxResults;
@end
